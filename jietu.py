#!/usr/bin/python
# -*- coding:UTF-8 -*-
import os
import sys
path = sys.path[0]
def exte(a,*endstring):
    array = map(a.endswith,endstring)
    if True in array:
        return True
    else:
        return False
 
def make_thumb():
    for dirpath,dirnames,filenames in os.walk(path):
        for name in filenames:
            if exte(name,type):
                try:
                    videopath = os.path.join(dirpath, name)
                    videoname = os.path.splitext(name)[0]
                    outputname = os.path.join(dirpath, videoname)+".jpg"
                    if not os.path.exists(outputname):
                        time = os.popen('ffmpeg -i %s 2>&1 | grep "Duration" | cut -d " " -f 4 | sed s/,//' %(videopath)).read().strip()
                        timeArr = time.split(":")
                        showtime = int((float(timeArr[0])*3600)+(float(timeArr[1])*60)+(float(timeArr[2])*1)-20)
                        num=4
                        tile1 = 2
                        tile2 = 2
                        if showtime>3600 and showtime<5400:
                            num=6
                            tile1 = 3
                            tile2 = 2
                        elif showtime>=5400:
                            num=9
                            tile1 = 3
                            tile2 = 3
                        
                        showtime2 = int(showtime/num+1)
                        print(str(showtime2)+"_"+str(num)+"_"+str(tile1)+"_"+str(tile2))

                        shell = 'ffmpeg -ss 20 -y  -i %s -vf "fps=1/%s,scale=iw/2:-1,tile=%sx%s" -f image2 %s' % (videopath, showtime2,tile1,tile2,outputname)
                        os.system(shell)
						
                except Exception:
                    print(name+"!!!!!!!!!!!!!!isbad!")
               
    return print('已合成',type,'文件的字幕')
	

 
types = ['.mp4','.avi','.wmv','.mkv','.flv','.ts','.mov','.m4v']
for type in types:
    make_thumb()